parser start {
    return parse_ethernet;
}

#define ETHERTYPE_VLAN 0x8100, 0x9100, 0x9200, 0x9300
#define ETHERTYPE_IPV4 0x0800
#define ETHERTYPE_IPV6 0x86dd
#define ETHERTYPE_ARP 0x0806
#define ETHERTYPE_RARP 0x8035
/* missing: vlan_3b, vlan_5b, ieee802_1q, ieee802_1ad */

header ethernet_t ethernet;

parser parse_ethernet {
    extract(ethernet);
    return select(latest.etherType) {
        ETHERTYPE_VLAN : parse_vlan;
        ETHERTYPE_IPV4 : parse_ipv4;
        ETHERTYPE_IPV6 : parse_ipv6;
        ETHERTYPE_ARP : parse_arp_rarp;
        ETHERTYPE_RARP : parse_arp_rarp;
        default : ingress;
    }
}

#define VLAN_DEPTH 4
header vlan_tag_t vlan_tag_[VLAN_DEPTH];
header vlan_tag_3b_t vlan_tag_3b[VLAN_DEPTH];
header vlan_tag_5b_t vlan_tag_5b[VLAN_DEPTH];

parser parse_vlan {
    extract(vlan_tag_[next]);
    return select(latest.etherType) {
        ETHERTYPE_VLAN : parse_vlan;
        ETHERTYPE_IPV4 : parse_ipv4;
        ETHERTYPE_IPV6 : parse_ipv6;
        ETHERTYPE_ARP : parse_arp_rarp;
        ETHERTYPE_RARP : parse_arp_rarp;
        default : ingress;
    }
}

#define IP_PROTOCOLS_ICMP 1
#define IP_PROTOCOLS_TCP 6
#define IP_PROTOCOLS_UDP 17
#define IP_PROTOCOLS_GRE 47
#define IP_PROTOCOLS_IPSEC_ESP 50
#define IP_PROTOCOLS_IPSEC_AH 51
#define IP_PROTOCOLS_ICMPV6 58
#define IP_PROTOCOLS_SCTP 132

header ipv4_t ipv4;

parser parse_ipv4 {
    extract(ipv4);
    return select(latest.fragOffset, latest.protocol) {
        IP_PROTOCOLS_ICMP : parse_icmp;
        IP_PROTOCOLS_TCP : parse_tcp;
        IP_PROTOCOLS_UDP : parse_udp;
        IP_PROTOCOLS_GRE : parse_gre;
        IP_PROTOCOLS_IPSEC_ESP : parse_ipsec_esp;
        IP_PROTOCOLS_SCTP : parse_sctp;
        default : ingress;
    }
}

header ipv6_t ipv6;

parser parse_ipv6 {
    extract(ipv6);
    return select(latest.nextHdr) {
        IP_PROTOCOLS_ICMPV6 : parse_icmpv6;
        IP_PROTOCOLS_TCP : parse_tcp;
        IP_PROTOCOLS_UDP : parse_udp;
        IP_PROTOCOLS_GRE : parse_gre;
        IP_PROTOCOLS_IPSEC_ESP : parse_ipsec_esp;
        IP_PROTOCOLS_SCTP : parse_sctp;
        default : ingress;
    }
}

header icmp_t icmp;

parser parse_icmp {
    extract(icmp);
    return ingress;
}

header icmpv6_t icmpv6;

parser parse_icmpv6 {
    extract(icmpv6);
    return ingress;
}

header tcp_t tcp;

parser parse_tcp {
    extract(tcp);
    return ingress;
}

#define UDP_PORT_VXLAN 4789

header udp_t udp;

parser parse_udp {
    extract(udp);
    return select(latest.dstPort) {
        UDP_PORT_VXLAN : parse_vxlan;
        default : ingress;
    }
}

header sctp_t sctp;

parser parse_sctp {
    extract(sctp);
    return ingress;
}

#define GRE_PROTOCOLS_NV_GRE_INNER 0x16558
#define GRE_PROTOCOLS_GRE 0x16559

#define GRE_DEPTH 3

header gre_t gre[GRE_DEPTH];

parser parse_gre {
    extract(gre[next]);
    return select(latest.K, latest.proto) {
        GRE_PROTOCOLS_NV_GRE_INNER : parse_nv_gre_inner;
        GRE_PROTOCOLS_GRE : parse_gre;
        default : ingress;
    }
}

header nv_gre_inner_t nv_gre_inner;
header ethernet_t inner_ethernet;

parser parse_nv_gre_inner {
    extract(nv_gre_inner);
    extract(inner_ethernet);
    return ingress;
}

header ipsec_esp_t ipsec_esp;

parser parse_ipsec_esp {
    extract(ipsec_esp);
    return ingress;
}

#define ARP_PROTOTYPES_ARP_RARP_IPV4 0x0800

header arp_rarp_t arp_rarp;

parser parse_arp_rarp {
    extract(arp_rarp);
    return select(latest.protoType) {
        ARP_PROTOTYPES_ARP_RARP_IPV4 : parse_arp_rarp_ipv4;
        default : ingress;
    }
}

header arp_rarp_ipv4_t arp_rarp_ipv4;

parser parse_arp_rarp_ipv4 {
    extract(arp_rarp_ipv4);
    return ingress;
}

header vxlan_t vxlan;

parser parse_vxlan {
    extract(vxlan);
    extract(inner_ethernet);
    return ingress;
}
