#include "includes/headers.p4"
#include "includes/parser.p4"

action hop(ttl, egress_port) {
    add_to_field(ttl, -1);
    modify_field(standard_metadata.egress_port, egress_port);
}

action hop_ipv4(egress_port) {
    hop(ipv4.ttl, egress_port);
}

action hop_ipv6(egress_port) {
    hop(ipv6.hopLimit, egress_port);
}

table ipv4_routing {
    reads {
        ipv4.dstAddr : lpm;
    }
    actions {
      drop;
      hop_ipv4;
    }
}

table ipv6_routing {
    reads {
        ipv6.dstAddr : lpm;
    }
    actions {
      drop;
      hop_ipv6;
    }
}

table drop_table {
    actions {
        drop;
    }
}

action rewrite_mac(srcmac, dstmac) {
    modify_field(ethernet.srcAddr, srcmac);
    modify_field(ethernet.dstAddr, dstmac);
}

table rewrite_ethernetframe{
    reads {
        standard_metadata.egress_port : exact;
    }
    actions {
        rewrite_mac;
    }
}

/* Main control flow */

control ingress {
    if(valid(ipv4)) {
        apply(ipv4_routing);
    }
    else {
        if(valid(ipv6)) {
            apply(ipv6_routing);
        }
    }
    
    if(ipv4.ttl == 0 or ipv6.hopLimit == 0) {
        apply(drop_table);
    }
}

control egress {
    apply(rewrite_ethernetframe);
}