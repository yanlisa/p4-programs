////////////////////////////////////////////////////////////////
// Parser functions and related definitions
////////////////////////////////////////////////////////////////
header ethernet_t ethernet;
header vlan_t vlan;
header ipv4_t ipv4;
header rcp_t rcp;

// Local metadata instance declaration
metadata per_packet_t per_packet;
metadata per_interval_t per_interval;
metadata register_local_var_t register_local_var;
metadata register_temp_t register_temp;

// Start with ethernet always.
parser start {
    return select(standard_metadata.instance_type) {
        trigger: ingress;
        default: ethernet;
    }
}

parser ethernet {
    extract(ethernet);   // Start with the ethernet header
    return select(latest.ethertype) {
        0x8100:     vlan;
        0x800:      ipv4;
        default:    ingress;
    }
}

parser vlan {
    extract(vlan);
    return select(latest.ethertype) {
        0x800:      ipv4;
        default:    ingress;
    }
}

parser ipv4 {
    extract(ipv4);
    return rcp;
}

parser rcp {
    extract(rcp);
    return ingress;  // All done with parsing; start matching
}
