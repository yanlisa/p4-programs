/////////////////////////
// actions.p4
////////////////////////

primitive_action get_queue_length(write_to_field);
primitive_action get_timestamp(write_to_field);

////// set metadata or read registers
action set_arrival_timestamp(value) {
    get_timestamp(per_packet.arrival_timestamp);
}
action update_end_time(value) {
    modify_field(register_local_var.end_time, value);
}
action read_interval_registers_ingress() {
    modify_field(per_interval.act_input_traffic, per_interval_r.act_input_traffic);
    modify_field(per_interval.traffic_spill, per_interval_r.traffic_spill);
    modify_field(per_interval.end_slot, per_interval_r.end_slot);
    modify_field(per_interval.avg_rtt, per_interval_r.avg_rtt);
    modify_field(per_interval.flow_rate, per_interval_r.flow_rate);
}
action read_interval_registers_egress() {
    get_queue_length(per_packet.queue_length);
}
action update_dequeue() {
    get_queue_length(per_packet.queue_length);
    get_timestamp(per_packet.curr_timestamp);
}

////// actual actions
////// interval modification (register modification)
action update_act_input_traffic(size) {
    add_to_field(per_interval.act_input_traffic, size);
}

action update_traffic_spill(size) {
    add_to_field(per_interval.traffic_spill, size);
}

action update_act_input_traffic_and_spill(new_act_input_traffic, new_traffic_spill) {
    update_act_input_traffic(new_act_input_traffic);
    update_traffic_spill(new_traffic_spill);
}

#define RTT_GAIN 0.02
action update_avg_rtt(new_avg_rtt) {
    modify_field(per_interval.avg_rtt, new_avg_rtt);
}

action update_interval(new_flow_rate, new_end_slot) {
    modify_field(per_interval.flow_rate = new_flow_rate;
    modify_field(per_interval.act_input_traffic, per_interval.traffic_spill);
    modify_field(per_interval.traffic_spill, 0);
    modify_field(per_interval.end_slot, new_end_slot);
}

///// packet modification
action set_rcp_packet_rate() {
    modify_field(rcp.rate, per_interval.flow_rate);
}
