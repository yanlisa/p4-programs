#include "parser.p4"
#include "tables.p4"

control ingress {
    apply_table(arrival_timestamp);
    apply_table(read_interval_registers_table);
    if (standard_metadata.instance_type != TRIGGER) {
	    apply_table(end_time);
	    if (per_packet.end_time <= per_interval.end_slot) {
            apply_table(act_input_traffic);
	    } else {
            apply_table(end_slot_update);
        }
    }
}

control egress {	
    apply_table(read_dequeue_table);

    if (standard_metadata.instance_type == TRIGGER) {
        apply_table(interval_update); // updates metadata
        // apply_table(flush_updates_to_register);
	else {
        if (rcp.packet_type == RCP_SYN || 
            rcp.packet_type == RCP_REF || 
            rcp.packet_type == RCP_DATA) {
            if (rcp.rate < 0 ||
                    rcp.rate > per_interval.flow_rate) {
                apply_table(packet_rcp_rate_update);
            }
            apply_table(rtt_update);
    }
}
