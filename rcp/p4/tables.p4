#include "actions.p4"
#include "headers.p4"

table arrival_timestamp {
    actions {
        set_arrival_timestamp;
    }

table read_interval_registers_table {
    actions {
        read_interval_registers_ingress;
    }
}

table interval_update {
    reads {
        per_packet.curr_timestamp;
        per_packet.queue_length;
        per_interval.act_input_traffic;
        per_interval.avg_rtt;
        per_interval_flow_rate;
    }
    actions {
        update_interval;
    }
}

table end_time_update {
 reads {
       per_packet.arrival_timestamp;
       standard_metadata.packet_length;
 }
 actions {
       update_end_time;
 }
}

table act_input_traffic {
    actions {
        update_act_input_traffic;
    }
}

table end_slot_update {
    reads {
        per_packet.arrival_timestamp;
        per_interval.end_slot;
        standard_metadata.packet_length;
    }
    actions {
        update_act_input_traffic_and_splill;
    }
}

table rtt_update {
    reads {
        rcp.rtt;
        per_interval.avg_rtt;
    }
    actions {
        update_avg_rtt;
    }
}

table packet_rcp_rate_update {
    actions {
        set_rcp_packet_rate;
    }
}

table read_dequeue {
    actions {
        read_dequeue;
    }
}
