////////////////////////
// headers.p4
////////////////////////

// standard_metadata:
// ingress_port, packet_length
// egress_spec, egress_port, egress_instance
// instance_type (normal, ingr/egr clone, recirculated)
// parser_status, parser_error_location

header_type per_packet_t {
    fields {	      
	    arrival_timestamp : 48; // call get_timestamp;
        queue_length : 24; // call get_queue_length;
	    enqueue_timestamp: 48; // available in egress
        curr_timestamp : 48; // call get_timestamp;
	    end_time : 48; // to calculate
	}
}

header_type per_interval_t {
    fields {
 	    act_input_traffic : 32;
	    traffic_spill : 32;
	    end_slot : 32;
	    avg_rtt : 32;
	    flow_rate : 32;
	}
}

header_type rcp_t {
    fields {
     	packet_type : 8;
    	rate : 32;
    	rtt : 32;
	}
}

register per_interval_r {
	 layout : per_interval_t;
	 static;
	 instance_count: 1;
}

header_type ethernet_t {
    fields {
        dstAddr : 48;
        srcAddr : 48;
        etherType : 16;
    }
}

header_type vlan_tag_t {
    fields {
        pcp : 3;
        cfi : 1;
        vid : 12;
        etherType : 16;
    }
}

header_type ipv4_t {
    fields {
        version : 4;
        ihl : 4;
        diffserv : 8;
        totalLen : 16;
        identification : 16;
        flags : 3;
        fragOffset : 13;
        ttl : 8;
        protocol : 8;
        hdrChecksum : 16;
        srcAddr : 32;
        dstAddr: 32;
    }
    length : ihl * 4;
}
