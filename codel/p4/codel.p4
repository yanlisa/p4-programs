// EGRESS: SETTING per_packet.okay_to_drop

/* bool ok_to_drop; */

/* if (!skb) { */
/*   vars->first_above_time = 0; */
/*   return false; */
/*  } */

/* vars->ldelay = now - codel_get_enqueue_time(skb); */
/* sch->qstats.backlog -= qdisc_pkt_len(skb); */
/* per_packet.ldelay, per_packet.queue set by switch */

/* if (unlikely(qdisc_pkt_len(skb) > stats->maxpacket)) */
/*   stats->maxpacket = qdisc_pkt_len(skb); */
if (per_packet.packet_length > stats.maxpacket) {
  modify_field(stats.maxpacket, per_packet.packet_length);
    }


/* if (codel_time_before(vars->ldelay, params->target) || */
/*     sch->qstats.backlog <= stats->maxpacket) { */
/*   /\* went below - stay below for at least interval *\/ */
/*   vars->first_above_time = 0; */
/*   return false; */
/*  } */
if (per_packet.ldelay > params.target || per_packet.queue <= stats.maxpacket) {
  modify_field(vars.first_above_time, 0);
  modify_field(per_packet.okay_to_drop, 1);
 } else {

/* ok_to_drop = false; */
/* if (vars->first_above_time == 0) { */
/*   /\* just went above from below. If we stay above */
/*    * for at least interval we'll say it's ok to drop */
/*    *\/ */
/*   vars->first_above_time = now + params->interval; */
  if (vars.first_above_time == 0) {
    add_to_field(vars.first_above_time, per_packet.dequeue_timestamp);
    add_to_field(vars.first_above_time, PARAMS.INTERVAL);
    modify_field(per_packet.okay_to_drop, 0);
/*  } else if (codel_time_after(now, vars->first_above_time)) { */
/*   ok_to_drop = true; */
  } else if (per_packet.dequeue_timestamp > vars.first_above_time) {
    modify_field(per_packet.dequeue_timestamp, 1);
  }
 }
/*  } */
/* return ok_to_drop; */
// MIGHT WANT TO REWRITE (OPTIMIZE CODE?) TO TAKE FEWER STAGES

control egress {
/*   static struct sk_buff *codel_dequeue(struct Qdisc *sch, */
/* 				       struct codel_params *params, */
/* 				       struct codel_vars *vars, */
/* 				       struct codel_stats *stats, */
/* 				       codel_skb_dequeue_t dequeue_func) */
/*   { */

/*   struct sk_buff *skb = dequeue_func(vars, sch); */
/*   codel_time_t now; */
/*   bool drop; */

/*   if (!skb) { */
/*     vars->dropping = false; */
/*     return skb; */
/*   } */
// WHEN NOTHING TO DEQUEUE, SET VARS.DROPPING TO FALSE!? 

/*   now = codel_get_time(); */

/*   drop = codel_should_drop(skb, sch, vars, params, stats, now); */
//   CODeL_SHOULD_DROP

/*   if (vars->dropping) { */
/*     if (!drop) { */
/*       /\* sojourn time below target - leave dropping state *\/ */
/*       vars->dropping = false; */
  if (vars.dropping == 1) {
    if (per_packet.okay_to_drop == 0) {
      modify_field(vars.dropping, 0);
/*     } else if (codel_time_after_eq(now, vars->drop_next)) { */
/*       /\* It's time for the next drop. Drop the current */
/*        * packet and dequeue the next. The dequeue might */
/*        * take us out of dropping state. */
/*        * If not, schedule the next drop. */
/*        * A large backlog might result in drop rates so high */
/*        * that the next drop should happen now, */
/*        * hence the while loop. */
/*        *\/ */
    } else if (per_packet.dequeue_timestamp >= vars.drop_next) {
      // NO MATTER WHAT WE DO WITH THIS PACKET, NEXT PACKET WILL
      // BE DEQUEUED ANYWAYS ACCORDING TO PIPELINE SCHEDULE??
      
/*       while (vars->dropping && */
/* 	     codel_time_after_eq(now, vars->drop_next)) { */
/* 	vars->count++; /\* dont care of possible wrap */
/* 			* since there is no more divide */
/* 			*\/ */
/* 	codel_Newton_step(vars); */
//    ALL P4 CODE IS PER-PACKET, BUT CODEL.H...
//    NO LOOPS IN P4
      if (vars.dropping == 1 && per_packet.dequeue_timestamp > vars.drop_next) {
	add_to_field(vars.count, 1);
	// SHIFT OPERATIONS ETC. 
	update_vars_rec_inv_sqrt();
/* 	if (params->ecn && INET_ECN_set_ce(skb)) { */
/* 	  stats->ecn_mark++; */
/* 	  vars->drop_next = */
/* 	    codel_control_law(vars->drop_next, */
/* 			      params->interval, */
/* 			      vars->rec_inv_sqrt); */
/* 	  goto end; */
/* 	} */
	if (params.ecn == 1) {
	  add_to_field(vars.ecn_mark, 1);
	  apply_table set_drop_next;
	} else {
/* 	  qdisc_drop(skb, sch); */
/* 	  stats->drop_count++; */
/* 	  skb = dequeue_func(vars, sch); */
// DROP AND DEQUEUE .. IN P4, THIS MEANS JUST SET DROP IN PACKET .. 
// NEXT PACKET ALREADY DEQUEUED AND STARTED IN SOME EGRESS ANYWAYS
	  modify_field(stats.drop_count, 1);
/* 	  if (!codel_should_drop(skb, sch, */
/* 				 vars, params, stats, now)) { */
/* 	    /\* leave dropping state *\/ */
/* 	    vars->dropping = false; */
// THIS IS FOR NEXT PACKET .. ??
// TO CONTINUE ..

	}
      } 
    }
}
