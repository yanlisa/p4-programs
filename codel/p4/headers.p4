////////////////////////
// headers.p4
////////////////////////

standard metadata {
 fields {
 	packet_length : 16
	}
}

header_type per_packet {
 fields {	      
	/* .. */
	arrival_timestamp : 48 // available at ingress
	enqueue_timestamp: 48 // available in egress
	dequeue_queue : 16 // available on dequeue
	end_time : 48 // to calculate
	}
}

header_type codel_vars {
 fields {
 	count : 32
	lastcount : 32
	dropping : 1
	revc_inv_sqrt : 16
	first_above_time : 48
	drop_next : 48
	ldelay : 48
	}
}

header_type codel_stats {
 fields {
 maxpacket : 32
 drop_count : 32
 ecn_mark : 32
 }
}

// ALREADY AVAILABLE IN PER_PACKET.
header_type qstats {
 fields {
 	backlog
 }
}
// OR CONSTANTS
header_type codel_stats {
 fields {
 interval : 32
 target : 32
 ecn : 1
 }
}

header_type rcp {
 fields {
 	packet_type : 8
	request_rate : 32
	flow_rate : 32
	rtt : 32
	}
}

header_type register_local_var {
 fields {
 	part1 = 32;
	part2 = 32;
	}
}

header_type register_temp {
fields {
 	temp1 = 32;
	temp2 = 32;
	temp3 = 32;
	}	
}

register codel_vars {
	 layout codel_vars;
	 static;
	 instance_count: 1;
}

// CAN ALSO JUST BE IN PACKET HEADER
// OR IN SWITCH METADATA
register temp {
	 layout register_temp;
	 static;
	 instance_count: 1
	 }

register local_var {
	 layout register_local_var;
	 static;
	 instance_count: 1
	 }
